﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public float lifeTime = 3.0f;
	public Damage damage;

	private float spawnTime = 0.0f;

	// Use this for initialization
	void Start () {
		spawnTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > spawnTime + lifeTime) {
			Despawn();
		}
	}

	void Despawn() {
		Destroy(gameObject);
	}
}
