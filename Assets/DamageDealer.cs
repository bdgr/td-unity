﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour {
	public DamageRoller damageRoller;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.transform.tag == "Creep") {
			Creep c = collision.gameObject.gameObject.GetComponentInChildren<Creep>();
			Damage damage = damageRoller.Roll();
			c.TakeDamage(damage, collision.contacts[0].point);
		}
	}
}
