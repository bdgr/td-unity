﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {
	public LayerMask mask;
	public ParticleSystem particles;
	public Transform projectileSpawn;
	public float chargeTimeMin = 2.0f;
	public float chargeTimeMax = 2.0f;
	public float projectileForceMin = 1.0f;
	public float projectileForceMax = 1.0f;
	public GameObject projectile;

	private GameObject target;
	private float chargeTime = 0.0f;
	private float fireTimestamp = 0.0f;
	private DamageRoller roller;
	public Vector3 groundNormal;

	// Use this for initialization
	void Start () {
		chargeTime = Random.Range(chargeTimeMin, chargeTimeMax);
		roller = GetComponent<DamageRoller>();
		if (!projectile) {
			projectile = Resources.Load("Cannonball") as GameObject;
		}
	}

	GameObject AquireTarget() {
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, 5, mask);
		if (hitColliders.Length > 0) {
			return hitColliders[0].gameObject;
		}
		return null;
	}

	public void SetGroundNormal(Vector3 n) {
		groundNormal = n;
		var rot = transform.Find("cannon/base");
		rot.rotation = Quaternion.LookRotation(n);
	}

	bool CanFire() {
		return Time.time > (fireTimestamp + chargeTime) && target != null;
	}
	
	// Update is called once per frame
	void Update () {
		target = AquireTarget();
		var rot = transform.Find("cannon/rotation");
		if (target) {
			var lookPos = target.transform.position - transform.position;
			rot.rotation = Quaternion.LookRotation(lookPos, groundNormal) * Quaternion.Euler(-90, 0, 0);
		} else {
			rot.rotation = Quaternion.Slerp(rot.transform.rotation, Quaternion.LookRotation(groundNormal), Time.deltaTime * 10f);
		}

		if (CanFire()) {
			particles.Play();
			var p = Instantiate(projectile, projectileSpawn.position, projectileSpawn.rotation);
			var projectileForce = Random.Range(projectileForceMin, projectileForceMax);
			(p.GetComponent("CreepDamage") as CreepDamage ).damage = roller.Roll();
			p.GetComponent<Rigidbody>().AddForce(rot.rotation * -Vector3.up * projectileForce);
			fireTimestamp = Time.time;
		}
	}
}
