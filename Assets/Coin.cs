﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
	public int value = 1;
	public float lifeTimeMin = 3.0f;
	public float lifeTimeMax = 5.0f;
	public Rigidbody rb;
	public Animator animator;
	public Gradient colors;

	public int maxValue = 5;

	private float lifeTime = 3.0f;
	private bool isDying = false;
	private float spawnTime;

	// Use this for initialization
	void Start () {
		var x = Random.Range(-1f, 1f);
		var y = Random.Range(4, 8);
		var z = Random.Range(-1f, 1f);
		rb.velocity = new Vector3(x, y, z);
		lifeTime = Random.Range(lifeTimeMin, lifeTimeMax);
		spawnTime = Time.time;
	}
	
	public void SetValue(int v) {
		GetComponentInChildren<Renderer>().material = GetMaterialFromValue(v);
	}

	Material GetMaterialFromValue(int v) {
		if (v >= 5) {
			return Resources.Load("coin-gold") as Material;
		} else if (v >= 2) {
			return Resources.Load("coin-silver") as Material;
		}
		return Resources.Load("coin-bronze") as Material;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > spawnTime + lifeTime && !isDying) {
			isDying = true;
			animator.Play("coin-collected");
		}
	}
	
	void Remove() {
		Manager.instance.gold += value;
		Destroy(gameObject);
	}
}
