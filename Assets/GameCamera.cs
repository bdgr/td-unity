﻿using System.Collections;
using UnityEngine;

public class GameCamera : MonoBehaviour {
	public GameObject target;
	public float smoothing = 5f;
	public float drag = 0.3f;
	public float pitchDrag = 0.1f;
	public Vector2 pitchLimits = new Vector2 (0f, 0f);
	public Vector3 offset = new Vector3 (3, 2.5f, -3);
	public Vector3 lookOffset = new Vector3 (0, 3, 0);
	public Vector3 buildModeOffset = new Vector3 (0, 20, 0);
	public Vector3 buildModeLook = new Vector3 (0, 0, 0);
	public Vector2 mouseSensitivity = new Vector2 (2.0f, 0.5f);
	public float buildModeSpeed = 2f;
	private float rotation = 0.0f;
	private float pitch = 0.0f;

	private Vector3 startPos;
	private bool dragging = false;
	private float startRotation = 0;
	private float startPitch = 0;
	private bool buildMode = false;
	private Vector3 targetPosition;
	private Vector3 lookTarget;
	private Vector3 up;
	private Vector3 buildModePosition = new Vector3 ();
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;
	}

	void HandleBuildMode () {
		targetPosition = buildModeOffset + buildModePosition;
		lookTarget = transform.position;
		lookTarget.y = 0;
		up = new Vector3 (1, 0, 0);
		var right = new Vector3 (0, 0, -1);
		if (Input.GetAxis ("Mouse ScrollWheel") != 0f) {
			buildModePosition.y -= Input.GetAxis ("Mouse ScrollWheel") * 6;
		}
		Vector3 horizontal = up * Input.GetAxis ("Vertical");
		Vector3 vertical = right * Input.GetAxis ("Horizontal");
		buildModePosition += (horizontal + vertical) * buildModeSpeed * Time.deltaTime;
	}

	void HandleGameMode () {
		if (dragging) {
			rotation = startRotation - (startPos.x - Input.mousePosition.x) * drag;
			pitch = startPitch + (startPos.y - Input.mousePosition.y) * pitchDrag;
			pitch = Mathf.Clamp (pitch, pitchLimits.x, pitchLimits.y);
		}
		float horizontal = Input.GetAxis ("Mouse X") * mouseSensitivity.x;
		float vertical = Input.GetAxis ("Mouse Y") * mouseSensitivity.y;
		pitch += vertical;
		pitch = Mathf.Clamp (pitch, pitchLimits.x, pitchLimits.y);
		rotation += horizontal;
		targetPosition = target.transform.position + Quaternion.Euler (0, rotation, 0) * offset;
		targetPosition.y += pitch;
		lookTarget = target.transform.position + lookOffset;
		up = Vector3.up;
	}

	void LateUpdate () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Cursor.lockState = CursorLockMode.None;
		}
		if (Input.GetButtonDown ("Fire3") && !buildMode) {
			dragging = true;
			startRotation = rotation;
			startPitch = pitch;
			startPos = Input.mousePosition;
		}
		if (Input.GetButtonUp ("Fire3")) {
			dragging = false;
		}
		if (Input.GetButtonDown ("BuildMode")) {
			buildMode = !buildMode;
			buildModePosition = transform.position;
			Manager.instance.buildMode = buildMode;
			dragging = false;
			if (!buildMode) {
				Cursor.lockState = CursorLockMode.Locked;
			} else {
				Cursor.lockState = CursorLockMode.None;
			}
		}
		if (buildMode) {
			HandleBuildMode ();
		} else {
			HandleGameMode ();
		}
		transform.position = Vector3.Lerp (transform.position, targetPosition, smoothing * Time.deltaTime);
		transform.LookAt (lookTarget, up);
	}
}