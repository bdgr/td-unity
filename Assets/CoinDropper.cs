﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinDropper : MonoBehaviour {
	public int minCoins = 1;
	public int maxCoins = 5;

	public GameObject coinObject;

	// Use this for initialization
	void Start () {
		if (!coinObject) {
			coinObject = Resources.Load("coin") as GameObject;
		}
	}

	int RollValue() {
		var tier = Random.Range(0, 3);
		switch(tier) {
			case 0:
			return 1;
			case 1:
			return 2;
			case 2:
			return 5;
		}
		return 1;
	}

	void SpawnCoin() {
		var c = Instantiate(coinObject, transform.position + new Vector3(0,1.0f, 0), Quaternion.identity);
		var coin = c.GetComponent<Coin>();
		coin.SetValue(RollValue());
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Drop() {
		var num = Random.Range(minCoins, maxCoins + 1);
		for (int i = 0; i < num; i++) {
			SpawnCoin();
		}
	}
}
