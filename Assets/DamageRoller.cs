﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageRoller : MonoBehaviour {
	public int damageMin = 2;
	public int damageMax = 4;
	public float critChance = 0.2f;
	public float critMultiplier = 1.5f;

	void Start () {
		
	}

	float RollDamage(bool isCrit) {
		float multiplier = 1.0f;
		if (isCrit) {
			multiplier = critMultiplier;
		}
		int roll = Random.Range(damageMin, damageMax + 1);
		return multiplier * roll;
	}

	public Damage Roll() {
		Damage d = new Damage();
		d.crit = Random.value < critChance;
		d.value = RollDamage(d.crit);
		return d;
	}
	
	void Update () {
		
	}
}
