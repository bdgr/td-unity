﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class BuildPreview : MonoBehaviour {

	public float size = 4;
	public int gridSize = 32;
	public Color colorBuildable = Color.blue;
	public Color colorEmpty = Color.white;
	public Color colorUnbuildable = Color.red;
	public LayerMask noBuildMask;

	private MeshFilter filter;
	// Use this for initialization
	void Start () {
		filter = GetComponent<MeshFilter> ();
		filter.mesh = GenerateMesh ();
	}

	private Mesh GenerateMesh () {
		Mesh m = new Mesh ();
		var verticies = new List<Vector3> ();
		var normals = new List<Vector3> ();
		var colors = new List<Color> ();
		var uvs = new List<Vector2> ();
		for (int x = 0; x < gridSize + 1; x++) {
			for (int y = 0; y < gridSize + 1; y++) {
				Vector3 v = new Vector3 (-size * 0.5f + size * (x / ((float) gridSize)), 0, -size * 0.5f + size * (y / ((float) gridSize)));
				Vector3 p = gameObject.transform.TransformPoint (v);
				p.y += 5;
				Ray ray = new Ray (p, Vector3.up * -1);
				RaycastHit hit;
				Color c = colorEmpty;
				Vector3 normal = Vector3.up;
				if (Physics.Raycast (ray, out hit, 100)) {
					NNInfoInternal n = AstarPath.active.graphs[0].GetNearest (hit.point);
					v = gameObject.transform.InverseTransformPoint(hit.point);
					v.y += 0.2f;
					var layer = hit.collider.gameObject.layer;
					var walkable = n.node.Walkable;
					if (walkable) {
						c = colorBuildable;
					}
					if (!walkable || noBuildMask.Contains(layer)) {
						c = colorUnbuildable;
					}
				}
				float alphaMod = 1;
				float dx = x - (gridSize / 2);
				float dy = y - (gridSize / 2);
				alphaMod = (dx * dx + dy * dy) / (gridSize * 2);
				if (alphaMod > 1) {
					alphaMod = 1;
				}
				c.a *= 1 - alphaMod;
				colors.Add (c);
				verticies.Add (v);
				normals.Add (normal);
				uvs.Add (new Vector2 (x / (float) gridSize, y / (float) gridSize));
			}
		}
		var triangles = new List<int> ();
		var vertCount = gridSize + 1;
		for (int i = 0; i < vertCount * vertCount - vertCount; i++) {
			if ((i + 1) % vertCount == 0) {
				continue;
			}
			triangles.AddRange (new List<int> () {
				i + 1 + vertCount, i + vertCount, i,
					i, i + 1, i + vertCount + 1
			});
		}
		m.SetVertices (verticies);
		m.SetNormals (normals);
		m.SetUVs (0, uvs);
		m.SetTriangles (triangles, 0);
		m.SetColors(colors);
		return m;
	}

	// Update is called once per frame
	void LateUpdate () {
		if (Manager.instance.buildMode) {
			filter.mesh = GenerateMesh();
		} else {
			transform.position = Vector3.down * 5;
		}
	}
}