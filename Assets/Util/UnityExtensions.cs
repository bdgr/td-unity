﻿ using UnityEngine.Events;
 using UnityEngine;

 public static class UnityExtensions {

 	public static bool Contains (this LayerMask mask, int layer) {
 		return mask == (mask | (1 << layer));
 	}

 	public static Vector3 RoundToNearestPointOnGrid (this Vector3 v, float size) {
 		int xCount = Mathf.RoundToInt (v.x / size);
 		int zCount = Mathf.RoundToInt (v.z / size);
		v.x = (float) xCount * size;
		v.z = (float) zCount * size;
		return v;
 	}
 }