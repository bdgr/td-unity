using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Manager : MonoBehaviour {
    public static Manager instance { get; private set; }

    public int gold = 30;
    public bool buildMode = false;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }
}