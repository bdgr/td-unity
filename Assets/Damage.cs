﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Damage {
	public float value;
	public bool crit;
}
