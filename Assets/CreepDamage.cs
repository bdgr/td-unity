﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepDamage : MonoBehaviour {
	public Damage damage;
	private bool dealtDamage = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision) {
		if (!dealtDamage && collision.transform.tag == "Creep") {
			dealtDamage = true;
			Creep c = collision.gameObject.gameObject.GetComponentInChildren<Creep>();
			c.TakeDamage(damage, collision.contacts[0].point);
		}
	}
}
