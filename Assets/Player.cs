﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	public float accel = 15.0f;
	public float speed = 15.0f;
	public float fallMultiplier = 1.5f;

	public Vector3 forward;
	public float sprint = 2;
	public Vector3 right;
	public float friction = 0.9f;
	public float idleFriction = 0.3f;
	public float jumpVelocity = 5.0f;
	public float doubleJumpVelocity = 3.5f;
	public float doubleJumpWindow = 2.0f;
	public int maxJumps = 2;
	public Collider physCollider;
	public GameObject spawn;

	private Vector3 acceleration;
	private Vector3 velocity;
	private Rigidbody rb;
	private CharacterController cc;
	private int jumps;
	private bool canJump = false;
	private Animator animator;
	private bool canAttack = true;
	private MeleeWeaponTrail weaponTrail;

	void Start () {
		jumps = maxJumps;
		cc = GetComponent<CharacterController> ();
		weaponTrail = GetComponentInChildren<MeleeWeaponTrail>();
		physCollider = transform.Find("sword").GetComponent<CapsuleCollider>();
		animator = GetComponent<Animator> ();
		forward = Camera.main.transform.forward.normalized;
		forward.y = 0;
		right = Quaternion.Euler (new Vector3 (0, 90, 0)) * forward;
	}

	bool CanDoubleJump () {
		return !cc.isGrounded &&
			velocity.y > -doubleJumpWindow * 2 &&
			velocity.y < doubleJumpWindow;
	}

	void HandleAttack() {
		if (Input.GetButton("Fire1") && canAttack) {
			canAttack = false;
			physCollider.enabled = true;
			animator.Play("player-sword-attack");
		}
	}

	void DisableTrails() {
		canAttack = true;
		physCollider.enabled = false;
		weaponTrail.Emit = false;
	}

	void Update () {
		var speedMod = 1.0f;
		if (Input.GetButton("Sprint")) {
			speedMod = sprint;
		}
		forward = Camera.main.transform.forward.normalized;
		right = Quaternion.Euler (new Vector3 (0, 90, 0)) * forward;
		forward.y = 0;
		Vector3 horizontal = forward * Input.GetAxis ("Vertical");
		Vector3 vertical = right * Input.GetAxis ("Horizontal");
		Vector3 dir = horizontal + vertical;
		acceleration = dir.normalized * accel * speedMod;
		acceleration.y = Physics.gravity.y * Time.deltaTime;
		velocity += acceleration;
		if (Input.GetButtonDown ("Jump") && jumps > 0) {
			// Don't rely on cc.isGrounded
			if (canJump) {
				// Normal Jump
				canJump = false;
				jumps--;
				velocity.y = jumpVelocity;
			} else if (CanDoubleJump ()) {
				// Double Jump
				jumps--;
				velocity.y = doubleJumpVelocity;
			}
		}
		if (velocity.y < 0) {
			velocity.y += fallMultiplier * -9.8f * Time.deltaTime;
		} else if (velocity.y > 0 && !Input.GetButton ("Jump")) {
			velocity.y += fallMultiplier * -9.8f * Time.deltaTime;
		}
		if (cc.isGrounded && velocity.y < 0) {
			canJump = true;
			velocity.y = 0;
			jumps = maxJumps;
		}
		if (!Manager.instance.buildMode) {
			cc.Move (velocity * Time.deltaTime);
		}
		if (dir.magnitude < 0.3f) {
			velocity.x *= idleFriction;
			velocity.z *= idleFriction;
		}
		velocity.x *= friction;
		velocity.z *= friction;
		acceleration *= 0;

		Plane p = new Plane(Vector3.up, transform.position);
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		float rayDistance;
		if (p.Raycast(ray, out rayDistance)) {
			var lookTarget = ray.GetPoint(rayDistance);
			lookTarget.y = transform.position.y;
			transform.LookAt(lookTarget, new Vector3(0, 1, 0));
		}
		HandleAttack();
		if (transform.position.y < 0) {
			transform.position = spawn.transform.position;
		}
	}
}