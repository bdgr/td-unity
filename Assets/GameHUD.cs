﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHUD : MonoBehaviour {
	private Text gold;

	// Use this for initialization
	void Start () {
		gold = transform.Find("gold").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		gold.text = Manager.instance.gold.ToString();
	}
}
