﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class SeekerDestroy : MonoBehaviour {

	IAstarAI agent;

	void Start () {
		agent = GetComponent<IAstarAI> ();
	}

	void Update () {
		if (agent.reachedEndOfPath) {
			OnPathEnd();
		}
	}

	void OnPathEnd() {
		Destroy (gameObject);
	}
}