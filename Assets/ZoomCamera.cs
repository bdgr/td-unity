﻿using System.Collections;
using UnityEngine;

public class ZoomCamera : MonoBehaviour {
	private Camera camera;
	void Start () {
		camera = GetComponent<Camera> () as Camera;
		// c.transform.rotation = Quaternion.Euler(60, -45, 0);
	}

	void Update () {
		if (Input.GetAxis ("Mouse ScrollWheel") != 0f) {
			camera.orthographicSize -= Input.GetAxis ("Mouse ScrollWheel") * 6;
		}
	}
}