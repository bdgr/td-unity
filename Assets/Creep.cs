﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Creep : MonoBehaviour {
	public float healthMin = 20.0f;
	public float healthMax = 20.0f;
	public Vector3 healthBarOffset = new Vector3(0, 2, 0);

	private float health;
	private float startHealth;
	private GameObject damageText;
	private Slider slider;

	void Start () {
		startHealth = health = Random.Range(healthMin, healthMax);
		damageText = Resources.Load("CombatText") as GameObject;
		GameObject hb = Instantiate(Resources.Load("HealthBar"), transform.position, Quaternion.identity) as GameObject;
		hb.transform.parent = gameObject.transform;
		RectTransform rt = hb.GetComponent<RectTransform>();
		hb.transform.localPosition = healthBarOffset;
		slider = GetComponentInChildren<Slider>();
		UpdateSlider();
	}

	void UpdateSlider() {
		if (slider) {
			Debug.Log(health/startHealth);
			slider.value = health / startHealth;
		}
	}

	void Update () {
		
	}

	public void TakeDamage(Damage damage, Vector3 pos) {
		health -= damage.value;
		pos.y += 0.3f;
		GameObject dt = Instantiate(damageText, pos, Quaternion.identity) as GameObject;
		dt.transform.position = pos;
		DamageText text = dt.GetComponent("DamageText") as DamageText;
		text.SetDamage(damage);
		UpdateSlider();

		if (health <= 0) {
			CoinDropper cd = GetComponent<CoinDropper>();
			if (cd) {
				cd.Drop();
			}
			Destroy(gameObject.gameObject);
		}
	}
}
