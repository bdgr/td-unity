﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using Pathfinding.Util;
using UnityEngine;

public class Builder : MonoBehaviour {
    public LayerMask mask;
    public LayerMask buildPreviewMask;
    public GameObject tower;
    public GameObject start;
    public GameObject preview;
    public GameObject end;
    private GameObject lastSpawned;
    private float size = 1.5f;
    private bool pathDidError = false;

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        if (Manager.instance.buildMode) {
            var mousePos = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay (mousePos);
            RaycastHit hit;
            if (Physics.Raycast (ray, out hit, 1000, buildPreviewMask)) {
                preview.transform.position = hit.point + (Vector3.up * 0.2f);
                preview.transform.position = preview.transform.position.RoundToNearestPointOnGrid(0.5f);
            }
        }
        if (Input.GetButtonDown("Fire2")) {
            var mousePos = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay (mousePos);
            RaycastHit hit;
            if (Physics.Raycast (ray, out hit, 1000, mask)) {
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Towers")) {
                    DestroyImmediate(hit.collider.gameObject);
                    AstarPath.active.Scan();
                    return;
                }
                Vector3 spawn = hit.point;
                spawn = GetNearestPointOnGrid (spawn);
                spawn.y += 0.1f;
                lastSpawned = Instantiate (tower, spawn, Quaternion.identity);
                lastSpawned.GetComponent<Tower>().SetGroundNormal(hit.normal);
                AstarPath.active.Scan();
                Spawner[] spawners = FindObjectsOfType<Spawner>() as Spawner[];
                pathDidError = false;
                foreach (var spawner in spawners) {
                    var p = ABPath.Construct (spawner.transform.position, end.transform.position, OnPathComplete);
                    AstarPath.StartPath (p);
                }
            }
        }
    }

    public Vector3 GetNearestPointOnGrid (Vector3 position) {
        int xCount = Mathf.RoundToInt (position.x / size);
        int zCount = Mathf.RoundToInt (position.z / size);

        Vector3 result = new Vector3 (
            (float) xCount * size,
            position.y,
            (float) zCount * size);

        return result;
    }


    void OnPathComplete (Path p) {
        if (p.error && !pathDidError) {
            pathDidError = true;
            DestroyImmediate (lastSpawned);
            AstarPath.active.Scan ();
        }
    }
}