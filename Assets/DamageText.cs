﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : MonoBehaviour {
	public float lifeTime = 1.0f;
	public float speed = 0.01f;
	public Color critColor = Color.red;

	private Text text;
	// Use this for initialization
	void Start () {
	}

	public void SetDamage(Damage d) {
		text = GetComponentInChildren<Text>();
		text.text = d.value.ToString();
		if (d.crit) {
			transform.localScale += new Vector3(0.3f, 0.3f, 1);
			text.color = critColor;
			speed = 0.005f;
		}
	}
	
	// Update is called once per frame
	void Update () {
		lifeTime -= Time.deltaTime;
		if (lifeTime <= 0) {
			Destroy(gameObject);
		}
		transform.Translate(0, speed, 0);
		transform.LookAt(
			transform.position + Camera.main.transform.rotation * Vector3.forward,
			Camera.main.transform.rotation * Vector3.up
		);
	}
}
