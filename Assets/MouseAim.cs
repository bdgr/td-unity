﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAim : MonoBehaviour {
	public float rotateSpeed = 5;

    void Start() {
    }

    void LateUpdate() {
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
        float vertical = Input.GetAxis("Mouse Y") * rotateSpeed;
		Debug.Log(horizontal);
        gameObject.transform.RotateAround(transform.position, Vector3.up, horizontal);
        gameObject.transform.RotateAround(transform.position, Vector3.left, vertical);
    }
}
