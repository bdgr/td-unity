﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Spawner : MonoBehaviour {

public GameObject goal;
	public GameObject creep;
	public GameObject creepBig;
	public float bigChance = 0.2f;
	public float radius = 2.0f;
	public GameObject pathHighlighter;
	public float pathHighlighterInterval = 10.0f;
	private float timer = 0.0f;
	// Use this for initialization
	void Start () {
		timer = pathHighlighterInterval;
		transform.localScale = new Vector3(radius + 1, 1, radius + 1);
	}

	void Spawn() {
		GameObject go = creep;
		if (Random.value < bigChance) {
			go = creepBig;
		}
		var offset = Random.insideUnitCircle * radius;
		var p = transform.position;
		p.x += offset.x;
		p.z += offset.y;
		var c = Instantiate(go, p, Quaternion.identity);
		Patrol patrol = c.GetComponent("Patrol") as Patrol;
		patrol.targets = new Transform[]{transform, goal.transform};
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Spawn")) {
			Spawn();
		}		
		timer -= Time.deltaTime;
		if (timer <= 0) {
			var c = Instantiate(pathHighlighter, transform.position, Quaternion.identity);
			timer = pathHighlighterInterval;
			AIDestinationSetter path = c.GetComponent(typeof(AIDestinationSetter)) as AIDestinationSetter;
			path.target = goal.transform;
		}
	}
}
